/* Kanye Discord Bot. Raven Scott 12.19.2020 */

/* How to install:

npm install

- (https://discord.com/developers/applications)
- Generate a bot token 
- How to add  a bot to a discord server (https://discordapi.com/permissions.html#2146958847)
- General Bot Structure, Ping/Pong Explanation (https://discord.js.org/)
- Teaching how to use NPM and include libraries for use (http://npmjs.com/)
- How to understand how to use example code from an NPM page (https://www.npmjs.com/package/unirest)
- Use an NPM (unirest) to request data from kanye.rest and parse its response
- Send the response back to discord. I.E the kanye quote will be displayed via chat.
*/

var unirest = require('unirest');
const Discord = require('discord.js');

const client = new Discord.Client();

let prefix = "!";

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  
  
    if (msg.content === 'kanye') {
   // Command Code Start
   unirest
   .get('https://api.kanye.rest')
   .then((response) => {
     console.log("The quote I sent was: " + "\"" + response.body.quote + "\"")
     msg.reply("\"" + response.body.quote + "\"" );
   })
  }


});

client.login('TOKENHERE');
